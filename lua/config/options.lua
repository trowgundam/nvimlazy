-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here

vim.opt.shiftwidth = 4
vim.opt.tabstop = 4
vim.opt.colorcolumn = "+1"
vim.opt.textwidth = 120

vim.o.guifont = "FiraCode Nerd Font:h10"
vim.g.neovide_transparency = 1.0
vim.g.neovide_floating_blur_amount_x = 8.0
vim.g.neovide_floating_blur_amount_y = 8.0

if vim.uv.os_uname().sysname == "Windows_NT" then
  vim.o.shell = vim.fn.executable("pwsh") == 1 and "pwsh" or "powershell"
  vim.o.shellcmdflag =
    "-NoLogo -NonInteractive -ExecutionPolicy RemoteSigned -Command [Console]::InputEncoding=[Console]::OutputEncoding=[System.Text.Encoding]::UTF8;$PSStyle.Formatting.Error = '';$PSStyle.Formatting.ErrorAccent = '';$PSStyle.Formatting.Warning = '';$PSStyle.OutputRendering = 'PlainText';"
  vim.o.shellredir = "2>&1 | Out-File -Encoding utf8 %s; exit $LastExitCode"
  vim.o.shellpipe = "2>&1 | Out-File -Encoding utf8 %s; exit $LastExitCode"
  vim.o.shellquote = ""
  vim.o.shellxquote = ""
end

local hostname = string.lower(vim.fn.hostname())
if hostname == "mali-cratis" then
  vim.g.neovide_scale_factor = 1.333
  vim.g.neovide_refresh_rate = 165
  vim.g.neovide_refresh_rate_idle = 21
elseif hostname == "mali-dei" then
  vim.g.neovide_scale_factor = 1.5
  vim.g.neovide_refresh_rate = 120
  vim.g.neovide_refresh_rate_idle = 15
else
  vim.g.neovide_scale_factor = 1.0
  vim.g.neovide_refresh_rate = 60
  vim.g.neovide_refresh_rate_idle = 15
end
