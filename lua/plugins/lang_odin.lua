if vim.fn.executable("odin") == 1 then
  return {
    {
      "neovim/nvim-lspconfig",
      ---@type PluginLspOpts
      opts = {
        servers = {
          ols = {},
        },
      },
    },
    {
      "williamboman/mason.nvim",
      ---@type MasonLspconfigSettings
      opts = {
        ensure_installed = { "ols" },
      },
    },
    {
      "nvim-treesitter/nvim-treesitter",
      ---@type TSConfig
      opts = {
        ensure_installed = { "odin" },
      },
    },
  }
else
  return {}
end
