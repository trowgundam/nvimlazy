return {
  {
    "neovim/nvim-lspconfig",
    ---@type PluginLspOpts
    opts = {
      servers = {
        lua_ls = {
          settings = {
            Lua = {
              runtime = {
                version = "LuaJIT",
              },
              workspace = {
                checkThirdParty = true,
                library = vim.api.nvim_get_runtime_file("", true),
              },
              diagnostics = {
                disable = { "missing-fields" },
                globals = { "vim" },
              },
              doc = {
                privateName = { "^_" },
              },
              hint = {
                setType = true,
              },
            },
          },
        },
      },
    },
  },
  {
    "nvim-treesitter/nvim-treesitter",
    ---@type TSConfig
    opts = {
      ensure_installed = { "luap" },
    },
  },
}
