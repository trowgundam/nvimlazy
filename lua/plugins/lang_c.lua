local config = {
  {
    "nvim-treesitter/nvim-treesitter",
    ---@type TSConfig
    opts = {
      ensure_installed = { "cpp" },
    },
  },
}

if vim.fn.executable("cmake") then
  vim.list_extend(config, {
    {
      "nvim-treesitter/nvim-treesitter",
      ---@type TSConfig
      opts = {
        ensure_installed = { "cmake" },
      },
    },
  })
end

return config
