---@return integer
local function get_fps()
  local host_name = string.lower(vim.uv.os_gethostname())
  if host_name == "mali-dei" then
    return 120
  elseif host_name == "mali-cratis" then
    return 165
  else
    return 60
  end
end

return {
  {
    "folke/snacks.nvim",
    ---@type snacks.Config
    opts = {
      animate = {
        fps = get_fps(),
      },
      statuscolumn = {
        enabled = true,
      },
      terminal = {
        ---@diagnostic disable-next-line: undefined-field
        shell = vim.uv.os_uname().sysname == "Windows_NT"
            and (vim.fn.executable("pwsh") == 1 and "pwsh.exe -NoLogo" or "powershell.exe -NoLogo")
          or nil,
      },
      words = {
        enabled = true,
      },
    },
  },
}
