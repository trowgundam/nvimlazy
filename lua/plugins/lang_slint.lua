if vim.fn.executable("odin") == 1 then
  return {
    {
      "neovim/nvim-lspconfig",
      ---@type PluginLspOpts
      opts = {
        servers = {
          slint_lsp = {},
        },
      },
    },
    {
      "williamboman/mason.nvim",
      ---@type MasonLspconfigSettings
      opts = {
        ensure_installed = { "slint-lsp" },
      },
    },
    {
      "nvim-treesitter/nvim-treesitter",
      ---@type TSConfig
      opts = {
        ensure_installed = { "slint" },
      },
    },
  }
else
  return {}
end
