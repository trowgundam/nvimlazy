if vim.fn.executable("pwsh") == 1 then
  return {
    {
      "williamboman/mason.nvim",
      ---@type MasonLspconfigSettings
      opts = {
        ensure_intalled = { "powershell-editor-services" },
      },
    },
    {
      "neovim/nvim-lspconfig",
      ---@type PluginLspOpts
      opts = {
        servers = {
          powershell_es = {
            -- bundle_path = vim.fn.stdpath("data")
            --   .. "/mason/packages/powershell-editor-services/PowerShellEditorServices/",
          },
        },
      },
    },
  }
else
  return {}
end
