if vim.fn.executable("go") == 1 then
  return {
    {
      "nvim-treesitter/nvim-treesitter",
      ---@type TSConfig
      opts = {
        ensure_installed = { "gotmpl" },
      },
    },
  }
else
  return {}
end
