return {
  {
    "nvim-treesitter/nvim-treesitter",
    ---@type TSConfig
    opts = {
      ensure_installed = {
        "comment",
        "css",
        "csv",
        "dart",
        "glsl",
        "gpg",
        "hlsl",
        "html",
        "http",
        "ini",
        "jq",
        "just",
        "llvm",
        "make",
        "scss",
        "ssh_config",
        "templ",
        "tmux",
        "udev",
        "wgsl",
        "wgsl_bevy",
        "yuck",
      },
    },
  },
}
