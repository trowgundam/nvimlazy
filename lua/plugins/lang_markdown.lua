return {
  ---@type LazySpec
  {
    "opdavies/toggle-checkbox.nvim",
    lazy = true,
    ft = "markdown",
    ---@type LazyKeysSpec[]
    keys = { { "<leader>tt", ":lua require('toggle-checkbox').toggle()<cr>", desc = "Toggle Checkbox" } },
  },
}
