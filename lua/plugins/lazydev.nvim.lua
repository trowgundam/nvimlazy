return {
  {
    "folke/lazydev.nvim",
    ---@type lazydev.Config
    opts = {
      library = {
        "lazy.nvim",
        { path = "${3rd}/luv/library", words = { "vim%.uv" } },
        { path = "catppuccin", words = { "Catppuccin" } },
        { path = "lazydev.nvim", words = { "lazydev" } },
        { path = "LazyVim", words = { "LazyVim" } },
        { path = "mason-lspconfig.nvim", words = { "Mason" } },
        { path = "nvim-treesitter", words = { "TS" } },
        { path = "snacks.nvim", words = { "snacks" } },
      },
    },
  },
}
