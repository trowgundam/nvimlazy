if vim.fn.executable("opam") == 1 then
  return {
    { import = "lazyvim.plugins.extras.lang.ocaml" },
  }
else
  return {}
end
