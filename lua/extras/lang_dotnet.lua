if vim.fn.executable("dotnet") == 1 then
  return {
    { import = "lazyvim.plugins.extras.lang.omnisharp" },
  }
else
  return {}
end
