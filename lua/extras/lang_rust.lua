if vim.fn.executable("cargo") == 1 then
  return {
    { import = "lazyvim.plugins.extras.lang.rust" },
  }
else
  return {}
end
