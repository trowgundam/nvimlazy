if vim.fn.executable("go") == 1 then
  return {
    { import = "lazyvim.plugins.extras.lang.go" },
  }
else
  return {}
end
