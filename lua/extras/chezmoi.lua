---@diagnostic disable-next-line: undefined-field
if vim.loop.os_uname().sysname == "Linux" and vim.fn.executable("chezmoi") == 1 then
  return {
    { import = "lazyvim.plugins.extras.util.chezmoi" },
  }
else
  return {}
end
