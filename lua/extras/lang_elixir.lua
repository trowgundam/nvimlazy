if vim.fn.executable("elixir") == 1 then
  return {
    { import = "lazyvim.plugins.extras.lang.elixir" },
  }
else
  return {}
end
