if vim.fn.executable("git") == 1 then
  return {
    { import = "lazyvim.plugins.extras.lang.git" },
  }
else
  return {}
end
