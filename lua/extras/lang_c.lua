local config = {}

if vim.fn.executable("clangd") then
  vim.list_extend(config, {
    { import = "lazyvim.plugins.extras.lang.clangd" },
  })
end

if vim.fn.executable("cmake") then
  vim.list_extend(config, {
    { import = "lazyvim.plugins.extras.lang.cmake" },
  })
end

return config
