if vim.fn.executable("zig") == 1 then
  return {
    { import = "lazyvim.plugins.extras.lang.zig" },
  }
else
  return {}
end
