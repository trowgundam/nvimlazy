if vim.fn.executable("npm") == 1 then
  return {
    { import = "lazyvim.plugins.extras.lang.tailwind" },
  }
else
  return {}
end
