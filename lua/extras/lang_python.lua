if vim.fn.executable("python") == 1 then
  return {
    { import = "lazyvim.plugins.extras.lang.python" },
  }
else
  return {}
end
