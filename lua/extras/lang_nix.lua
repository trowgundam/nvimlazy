if vim.fn.executable("nix") == 1 then
  return {
    { import = "lazyvim.plugins.extras.lang.nix" },
  }
else
  return {}
end
